# Install the GPG key
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg

# Select the stable channel to use
sudo yum-config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo

# Update yum and install Sublime Text
sudo yum install sublime-text
